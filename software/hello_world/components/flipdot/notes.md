Flipdot library stores internal framebuffer to do diffing.
Flipdot library provides drawing primitives to manipulate the framebuffer.
All internals are exposed for custom applications.
